package se.intem.itext

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

class ITextLicenseLoaderTest : LicenseTestSupport() {

    @Autowired
    private lateinit var loader: ITextLicenseLoader

    @Test
    fun autowired() {
        Assertions.assertNotNull(loader)
    }

    @Test
    fun sample_license_should_be_found() {
        Assertions.assertEquals(ITextLicenseMode.licensed, loader.mode)
    }


}
