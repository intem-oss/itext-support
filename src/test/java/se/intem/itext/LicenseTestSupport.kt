package se.intem.itext

import configuration.se.intem.itext.ITextLicenseConfiguration
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.TestPropertySource
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@ContextConfiguration(
    classes = [
        ITextLicenseConfiguration::class
    ]
)
@TestPropertySource("classpath:/license-test.properties")
open class LicenseTestSupport
