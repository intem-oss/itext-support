package configuration.se.intem.itext

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import se.intem.itext.ITextLicenseLoader

@Configuration
@ComponentScan(
    basePackageClasses = [
        ITextLicenseLoader::class
    ]
)
class ITextLicenseConfiguration {
}
