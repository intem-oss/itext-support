package se.intem.itext

import com.itextpdf.licensekey.LicenseKey
import com.itextpdf.licensekey.LicenseKeyException
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.ApplicationListener
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.core.io.Resource
import org.springframework.stereotype.Component
import se.intem.itext.ITextLicenseMode.*
import java.util.Optional

@Component
class ITextLicenseLoader(
    @Value(value = "\${itext.license.key.core:#{null}}")
    private val coreLicenseKey: Optional<Resource>,
    @Value(value = "\${itext.license.key.html:#{null}}")
    private val htmlLicenseKey: Optional<Resource>,
) : ApplicationListener<ContextRefreshedEvent> {

    var mode: ITextLicenseMode? = null
        set(value) {
            // Block transitioning from agpl to licensed
            if (field != agpl) {
                field = value
            }
        }
        get() = if (field == null) agpl else field

    private fun loadLicenseKeys() {

        log.info("Loading iText license keys...")

        val keys = listOf(coreLicenseKey, htmlLicenseKey)
            .filter { it.isPresent }
            .map { it.get() }
            .distinct() // skip second resource if provided xml contains both licenses

        val loaded = keys
            .count { loadLicenseKey(it) }

        log.info("$loaded/${keys.size} license keys successfully loaded")
    }

    private fun loadLicenseKey(resource: Resource): Boolean {
        try {
            log.info("Loading license key from $resource...")

            if (!resource.exists()) {
                throw LicenseKeyException("Resource $resource does not exist")
            }

            val input = resource.inputStream
            LicenseKey.loadLicenseFile(input)
            this.mode = licensed
            return true

        } catch (e: LicenseKeyException) {
            log.error("Failed to load license $resource", e)
            this.mode = agpl
            return false
        }
    }

    companion object {
        /** Logger for this class. */
        private val log = KotlinLogging.logger {}
    }

    override fun onApplicationEvent(event: ContextRefreshedEvent) {
        loadLicenseKeys()
    }

}
