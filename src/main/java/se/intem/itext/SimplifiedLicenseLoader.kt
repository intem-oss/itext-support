package se.intem.itext

import com.itextpdf.licensekey.LicenseKey
import com.itextpdf.licensekey.LicenseKeyException
import java.io.File

class SimplifiedLicenseLoader(vararg keys: File) {

    init {
        val loaded = keys.count { load(it) }

        println("$loaded/${keys.size} license keys successfully loaded")
    }

    private fun load(resource: File): Boolean {
        try {
            println("Loading license key from $resource...")

            if (!resource.exists()) {
                throw LicenseKeyException("Resource $resource does not exist")
            }

            LicenseKey.loadLicenseFile(resource.absolutePath)
            return true

        } catch (e: LicenseKeyException) {
            System.err.println(("Failed to load license $resource: $e"))
            return false
        }

    }

    companion object {

        @JvmStatic
        fun main(args: Array<String>) {
            SimplifiedLicenseLoader(File("core.xml"), File("html-2.0.xml"))
        }

    }
}
