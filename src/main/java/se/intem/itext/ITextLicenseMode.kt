package se.intem.itext

enum class ITextLicenseMode {
    licensed,
    agpl
}
